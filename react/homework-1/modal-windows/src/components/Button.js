import React, {Component} from 'react';
import './Button.scss';

class Button extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: true
        }
    }

    render() {
        // console.log('button', this.props);
        return (
            <button className='start-btn'
                onClick={this.props.onBC}
                style={{backgroundColor: this.props.items.backgroundColor}}>
                {this.props.items.text}</button>
        )
    }

}
export default Button;