import React, {Component} from 'react';
import './Modal.scss';

class Modal extends Component {
    constructor(props) {
        super(props)

        this.state = {
            visible: true
        }

    }

    closeModal = () => {
        this.setState(
            {visible: false}
        )
    }

    openModal = () => {
        this.setState(
            {visible: true}
        )
    }
    render() {
        // let elem = this.props.inst;
        // if (!this.state.visible)
        //     return;
        // onClick={() => this.setState({visible: false})}
        // return
        // (
        //     <div className='modal-container' >
        //         <div className='modal-window' style={{backgroundColor: elem.backgroundColor}}>
        //             <div className='modal-header' style={{backgroundColor: elem.hbackgroundColor}}>
        //                 <div className='modal-title'>{elem.header}</div>
        //                 <div className='modal-close' style={{display: elem.closeButton ? 'block' : 'none'}} onClick={elem.closeButton}>X</div>
        //             </div>
        //             <div className='modal-body'>{elem.text}</div>
        //             <div className='modal-btn'>
        //                 <button className='ok-btn' style={{backgroundColor: elem.hbackgroundColor}}>{elem.actions.posansw}</button>
        //                 <button className='no-btn' style={{backgroundColor: elem.hbackgroundColor}}>{elem.actions.negansw}</button>
        //             </div>
        //         </div>
        //     </div>
        // )

        let elem = this.props.inst;

        let mw = this.state.visible && this.props.isOpen &&
            <div className='modal-container' onClick={this.closeModal}>
                <div className='modal-window' style={{backgroundColor: elem.backgroundColor}}>
                    <div className='modal-header' style={{backgroundColor: elem.hbackgroundColor}}>
                        <div className='modal-title'>{elem.header}</div>
                        <div className='modal-close' style={{display: elem.closeButton ? 'block' : 'none'}} onClick={elem.closeButton}>X</div>
                    </div>
                    <div className='modal-body'>{elem.text}</div>
                    <div className='modal-btn'>
                        <button className='ok-btn' style={{backgroundColor: elem.hbackgroundColor}}>{elem.actions.posansw}</button>
                        <button className='no-btn' style={{backgroundColor: elem.hbackgroundColor}}>{elem.actions.negansw}</button>
                    </div>
                </div>
            </div>
        return (

            <div>
                {mw}
            </div>
        )


    }
}
export default Modal;