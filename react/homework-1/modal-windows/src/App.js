import React, {Component} from 'react';
// import ReactDOM from 'react-dom';
import './index.css';
import Button from './components/Button';
import Modal from './components/Modal';


let button1 =
{
    backgroundColor: 'green',
    text: 'Open first modal'
};
let button2 =
{
    backgroundColor: 'red',
    text: 'Open second modal'
};

let modalsData1 =
{
    header: 'Do you want to delete this file?',
    closeButton: true,
    text: 'Once you delete this file, it won’t be possible to undo this action.Are you sure you want to delete it?',
    actions: {
        posansw: 'Ok',
        negansw: 'Cancel'
    },
    backgroundColor: '#e74c3c',
    hbackgroundColor: '#d44637'
};
let modalsData2 =
{
    header: 'Do you want to add this file?',
    closeButton: false,
    text: 'Are you sure you want to add it?',
    actions: {
        posansw: 'Ok',
        negansw: 'Cancel'
    },
    backgroundColor: 'rgb(104, 179, 30)',
    hbackgroundColor: 'rgb(80, 156, 4)'
};

class App extends Component {
    state = {
        isOpen1: false,
        isOpen2: false,
        // showBtn1: 'isOpen',
        // showBtn2: 'isOpen'
    }

    render() {
        return (
            <div>
                <div className='btn-container'>
                    <Button items={button1} onBC={this.changeIsOpen1} />
                    <Button items={button2} onBC={this.changeIsOpen2} />
                </div>
                <Modal inst={modalsData1} isOpen={this.state.isOpen1} />
                <Modal inst={modalsData2} isOpen={this.state.isOpen2} />

            </div>
        );
    }

    changeIsOpen1 = () => {

        this.setState(currentState => ({
            isOpen1: currentState.isOpen,
            isOpen2: !currentState.isOpen
        })
            // {

            // isOpen1: true,
            // isOpen2: false,
            // showBtn1: '!isOpen'

            // }
        )

    }

    changeIsOpen2 = () => {
        this.setState(currentState => ({
            isOpen1: !currentState.isOpen,
            isOpen2: currentState.isOpen
        })
        )

        // this.setState({
        //     isOpen1: false,
        //     isOpen2: true,
        //     // showBtn2: '!isOpen'
        // })
    }


}



export default App;