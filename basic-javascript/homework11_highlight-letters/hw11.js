let letters = document.querySelectorAll('.btn');
// console.log(letters);
let arr = {};
for (let i of letters) {
    arr[i.textContent.toUpperCase()] = i;
}
// console.log(arr);

let activeLetter;

function blueKeys(event) {
    // проверка что у нас есть такая кнопка на экране
    if (arr[event.key.toUpperCase()] == undefined) {
        return;
    }
    // обнулим цвет активного элемента если он есть
    if (activeLetter != undefined) {
        activeLetter.style.backgroundColor = 'black';
    }
    // установим активный элемент и устаноим ему цвет 
    activeLetter = arr[event.key.toUpperCase()];
    activeLetter.style.backgroundColor = 'blue';
}
window.addEventListener('keydown', blueKeys);



