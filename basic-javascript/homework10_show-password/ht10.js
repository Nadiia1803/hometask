
let icon = document.querySelectorAll('i');

icon.forEach(item => item.addEventListener('click', toggleIcon));

function toggleIcon() {

    this.classList.remove("fa-eye");
    this.classList.add("fa-eye-slash");

    let input = this.closest(".input-wrapper").querySelector('.secretword');
    if (input.type === "password") {
        input.type = "text";
    } else {
        input.type = "password";
    }
}

let button = document.querySelector('.btn');
button.addEventListener('click', comparePass);
function comparePass() {
    let enterPass = document.querySelector('#enterPass');
    let repeatPass = document.querySelector('#repeatPass');
    if (enterPass.value == repeatPass.value) {
        alert('You are welcome');
    } else {

        let out = document.querySelector(".val");
        out.innerHTML = "Нужно ввести одинаковые значения";
        out.style.color = 'red';
    }
}