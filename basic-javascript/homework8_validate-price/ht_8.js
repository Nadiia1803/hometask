 //1.Находим нужный элемент на HTML странице
let formField = document.querySelector('#price');

// 2. Вариант, когда поле находится в фокусе
formField.addEventListener('focus',focusInput);
function focusInput () {
    formField.style.borderColor = 'green';

}

// 3. Вариант, когда поле не в фокусе
 formField.addEventListener('blur',blurInput);
function blurInput(e) {
    formField.style.borderColor = 'white';
    // 4. Создан div для спана
    let div = document.createElement('div');
    div.id = 'hid';
    div.style.width = '350px';
    div.style.height = '50px';
    div.style.display = 'flex';
    div.style.border = '2px black solid';
    div.style.marginBottom = '30px';
    div.style.borderRadius = '20px';
    div.style.alignItems = 'center';
    div.style.justifyContent = 'center';

    // 5. Создан span
    let span = document.createElement('span');
    span.className = 'goods';
    span.innerHTML = `Текущая цена: ${e.target.value}`;
    span.style.height = '40px';
    span.style.width = '250px';
    span.style.display = 'flex';
    span.style.paddingLeft = '40px';
    span.style.fontSize = '20px';

    div.prepend(span);
    let button = document.createElement('button');

    button.innerHTML = 'X';
    button.style.height = '30px';
    button.style.width = '30px';
    button.style.display = 'flex';
    button.style.borderRadius = '30px';
    button.style.alignItems = 'center';
    button.style.justifyContent = 'center';
    button.style.fontWeight = 'bold';

    div.append(button);
    document.getElementById('main').prepend(div);

    // 6. окрашивание текста поля в зеленый цвет
    formField.style.color = 'green';
    // 7. Нажатие Х(кнопка закрытия) удаление спана и кнопки
    button.onclick = function (event) {
        event.target.parentElement.style.display = 'none';
        // 8. Обнуление поля ввода.
        document.querySelector('#price').value = '';
    };
}
// // 9.Если пользователь ввел число меньше 0

        let validField= document.getElementById('price');
        validField.addEventListener('blur', validation,false);
    function validation () {
            if (parseInt(validField.value)>0){
                validField.setCustomValidity('')
            }
            else {
                validField.setCustomValidity('Please enter correct price');
                validField.style.border = '2px red solid';
            }

        }
