

let images = document.querySelectorAll('.image-to-show');
let active = 0;
let slideInerval = setInterval(nextSlide, 3000);

function nextSlide() {
    images[active].classList.remove('active-img');
    if (active + 1 == images.length) {
        active = 0;
    } else {
        active++;
    }
    images[active].classList.add('active-img');

}

let stopBtn = document.querySelector('.stop');
let continueBtn = document.querySelector('.continue');

function pauseSlideshow() {
    clearInterval(slideInerval);
}

function playSlideshow() {
    slideInterval = setInterval(nextSlide, 3000);
}

stopBtn.onclick = pauseSlideshow;
continueBtn.onclick = playSlideshow;
