let arg1 = 0;
let arg2 = 0;
let math = "";


do {
    arg1 = parseInt(prompt('Enter your first number', arg1));
    arg2 = parseInt(prompt('Enter your second number', arg2));
    math = prompt('Enter type of math operation"+";"-","/" or "*', math);
}
while (controlNum(arg1) || controlNum(arg2) || !controlOp(math));

console.log(operation(arg1, arg2, math));

function operation(arg1, arg2, math) {

    if (math === '+') {
        return arg1 + arg2;

    } else if (math === "-") {
        return arg1 - arg2;

    } else if (math === "*") {
        return arg1 * arg2;

    } else if (math === "/") {
        return arg1 / arg2;

    }
}

function controlOp(input) { // correct input return true
    return input === "+" || input === '-' || input === "*" || input === '/';
}

function controlNum(input) { // correct input return false
    return typeof input !== 'number' || Number.isNaN(input);
}