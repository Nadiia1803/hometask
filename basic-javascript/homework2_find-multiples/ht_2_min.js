let n = prompt('Enter your number ');

let noNumbers = true;

for (let i = 0; i <= n; i++) {
    if (i !== 0 && i % 5 === 0) {
        console.log(i);
        noNumbers = false;
    }
}
if (noNumbers) {
    alert('Sorry, no numbers');
}