let changeBtn = document.querySelector(".changeBtn");

if (!localStorage.theme) localStorage.theme = "main-theme"
document.body.className = localStorage.theme;

changeBtn.addEventListener('click', (e) => {
    document.body.classList.toggle('other-theme');
    localStorage.theme = document.body.className || "main-theme"
});


