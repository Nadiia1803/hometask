const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

//1.  Добавление на страницу  `div` с `id="root"`.

let div = document.createElement('div');
div.setAttribute('id', "root");
document.body.append(div);

// 2. Добавление  на странцу тега 'ul'.
let ul = document.createElement('ul');
ul.innerHTML = 'books:'
div.appendChild(ul);

// 3. Перебрать элементы массива, отсоритровать объекты по свойствам, вывести на страницу в виде списка


const bookItems = books.map(function ({ author, name, price }) {
    return {
        'name': name,
        'author': author,
        'price': price,
        'all3prop': name != undefined && author != undefined && price != undefined,
        'toString': function () {
            return `${this.name}, ${this.author},${this.price}`
        }
    };
}
)
console.log(bookItems);


let listItemBook = bookItems.forEach(function (item, i, bi) {
    let obj = bookItems[i];
    try {
        if (obj.all3prop) {
            let li = document.createElement('li');
            li.innerHTML = obj.toString();
            ul.appendChild(li);
        }
        else {
            throw new SyntaxError("Данные неполны: нет одного из условий отбора " + obj.toString());

        }
    } catch (e) {
        console.log(e.message);

    }
})


// тоже рабочий код

// books.forEach(obj => {

//     try {
//         let tex = `${obj.name.toString()}, ${obj.author.toString()}, ${obj.price.toString()}`;
//         let li = document.createElement('li');
//         li.innerHTML = tex;
//         ul.appendChild(li);
//     } catch (err) {
//         console.log("Извините, книга не соответствует условиям отбора");
//     }
// });


// тоже рабочий код

// let listItemBook = books.forEach(function (item, i, books) {
//     let obj = books[i];

//     let hasPropertyAuthor = false;
//     let hasPropertyName = false;
//     let hasPropertyPrice = false;

//     for (let property in obj) {

//         if (property == 'author') {
//             hasPropertyAuthor = true;
//         } else if (property == 'name') {
//             hasPropertyName = true;
//         } else if (property == 'price') {
//             hasPropertyPrice = true;
//         }

//     }

//     if (hasPropertyPrice && hasPropertyAuthor && hasPropertyName) {
//         let tex = JSON.stringify(obj);
//         let li = document.createElement('li');
//         li.innerHTML = tex;
//         ul.appendChild(li);
//     }
//     else {
//         console.log("Извините, книга не соответствует условиям отбора")
//     }

// }
// )









