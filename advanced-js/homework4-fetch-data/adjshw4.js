let list = document.createElement('ul');
document.body.append(list);

// Запрос на получение названия фильмов 

let filmsList = fetch('https://swapi.dev/api/films/')
    .then(response => response.json())
    .then(data => {
        data.results.forEach(({
            title, episode_id, opening_crawl, characters
        }) => {
            const listItem = document.createElement('li');
            list.appendChild(listItem);

            const divFilmName = document.createElement('div');
            divFilmName.innerText = `название фильма:  ${title}`;
            listItem.appendChild(divFilmName);

            const divFilmHero = document.createElement('div');
            divFilmHero.innerText = `cписок персонажей: \n`;
            listItem.appendChild(divFilmHero);
            `cписок персонажей: \n`

            const divFilmId = document.createElement('div');
            divFilmId.innerText = `номер эпизода: ${episode_id}`;
            listItem.appendChild(divFilmId);

            const divFilmPlot = document.createElement('div');
            divFilmPlot.innerText = `короткое содержание: ${opening_crawl}`;
            listItem.appendChild(divFilmPlot);

            characters.forEach(elem => {
                fetch(elem)
                    .then(response => response.json())
                    .then(renderData)

                function renderData(data) {
                    divFilmHero.innerText += data.name + '\n';
                }

            });
        })
    })




    // .then(response => {
    //     response.results.forEach(({
    //         characters
    //     }) => {
    //         console.log(characters);
    //         // const listItem = document.createElement('li');
    //         // listItem.innerText = "название фильма: " + `${title} ` + '\n' + "номер эпизода: " + `${episode_id} ` + '\n' + "короткое содержание: " + `${opening_crawl} `;
    //         // list.appendChild(listItem);
    //     });
    // })
// Запрос на получение персонажей фильма

