class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    // Setters
    set addName(newName) {
        this.name = newName;
    }

    set addAge(newAge) {
        this.age = newAge;
    }

    set addSal(newSal) {
        this.salary = newSal;
    }

    // Getters

    get addName() {
        return `Имя: ${this.name}`;
    }

    get addAge() {
        return `Возраст:${this.age}`;
    }

    get addSal() {
        return `Доход: ${this.salary}`;
    }

}

let employee1 = new Employee('Ivan', 25, 35000);
let employee2 = new Employee('Dan', 33, 42000);
let employee3 = new Employee('Dima', 23, 16000);

console.log(employee1);

employee2.addName = 'Roman';
employee2.addAge = 37;
employee2.addSal = 31900;

console.log(employee2.addName);
console.log(employee2.addAge);
console.log(employee2.addSal);

console.log(employee3.addName);
console.log(employee3.addAge);
console.log(employee3.addSal);



class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get newSalary() {
        return this.salary * 3;
    }
}

let programmer1 = new Programmer('Pavel', 27, 33000, 'english');
console.log(programmer1);
let programmer2 = new Programmer('Vasiliy', 21, 11000, 'spain');
console.log(programmer2.newSalary);
let programmer3 = new Programmer('Alex', 29, 28000, 'franch')
console.log(programmer3.lang);